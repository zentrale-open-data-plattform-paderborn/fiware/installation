## Funding Information:
The results of this project were developed on behalf of the city of Paderborn within the funding of the digital model region Ostwestfalen-Lippe of the state of North Rhine-Westphalia.

![img](img/logoleiste.JPG)

## License:
Copyright © 2020 Profirator Oy, HYPERTEGRITY AG, omp computer gmbh. This work is licensed under a CC BY SA 4.0 [license](https://creativecommons.org/licenses/by-sa/4.0/). 
