# 1. Preface
Only start this document if [platform-setup](../platform-setup/README.md) has already been done.

This document is going to act as an end2end test for multiple user-interacted systems, where the users we create
will simulate the role of a data-consumer.

Also in order to see some internals of the system, we will deviate from the data-consumer path, 
and take i.e. the data-provider, and some admin roles into account.

> **_NOTE:_** While it is possible to cherry-pick some operations if you know what you are doing,
for a first run, it is strongly recommended to go through this document step by step. Otherwise you may
run into i.e. permission errors.

> **_NOTE2:_** This document assumes a clean system, so the harvester-cronjobs and secrets are not present.

## 1.1 Structure

You can read this document in 2 different ways: Either testing with or without real-world data.

If you choose to take real-world data into account, we will guide you on how to connect the AEMET Data Service 
to the platfrom and access historical data of this service.

**BUT:** You will need to give the system time to collect data. Effect of this will be in section 7, where you will 
encounter a break, asking you have to stop the test for several hours. So continue there at the next day.


### 1.1.1 With Realworld data
Just follow the document from Chapter 1 to chapter 10. You will have to do Chaper 9 twice, but it is stated where 
to go and where to go back.

### 1.1.2 Without Realworld data
Everything the AEMET Data Service will be skipped. This will include Chapter 6, 7 and 8 at a whole.

The only AEMET related topic, that should not be skipped will be Chapter 3.1. 

## 1.2 Prerequisites
1. Postman has to be installed on your device.
2. This Postman [collection](platform-testing/collections/Documentation.postman_collection.json) is imported to Postman.
3. Keyrock admin credentials are needed.
4. Umbrella admin credentials are needed.
5. Grafana admin credentials are needed.
5. Gitlab account with access to the Data Harvester is needed.
6. An ssh-client like PuTTY is needed
7. Kubernetes config-file is needed.
8. Two valid eMail adresses are needed for user creation.

# 2 Preparation
This document assumes you are logged out to all the systems in the beginning. So make sure you are NOT logged into at:
* [https://accounts.fiware.opendata-CITY.de](https://accounts.fiware.opendata-CITY.de)
* [https://apis.fiware.opendata-CITY.de](https://apis.fiware.opendata-CITY.de)
* [https://umbrella.fiware.opendata-CITY.de/admin](https://umbrella.fiware.opendata-CITY.de/admin)

## 2.1 Tenant User Creation
1.  Go to [https://accounts.fiware.opendata-CITY.de](https://accounts.fiware.opendata-CITY.de) and 
click the `Sign Up` button on the left.
2.  Choose any username (**Note: Use only alphanumeric only here [a-z, A-Z, 0-9] and do not use dots, dashes or white spaces in the username, there is a known issue about this**) and password
3.  Make sure you use a valid E-mail 
4.  Accept the FIWARE terms and conditions and hit `Sign up`.
5.  Wait for the Confirmation-E-Mail and click the confirmation link. (Hint: Check your spam-folder, too!)

6. Repeat step 1-5 for another user once.

> **_NOTE:_** The first user, will be referred as **USER1** and the second as **USER2**

> **_NOTE2:_** Depending on the Keyrock configuration, you may not get email notification to your inbox, but this will go do a defined Admin email. In this case, please manually activate the user in Keyrock, or ask your Keyrock admin for activation.

## 2.2 Tenant User Promotion

> **_NOTE:_** These steps would be done by the keyrock admin.

1.  Go to [https://accounts.fiware.opendata-CITY.de](https://accounts.fiware.opendata-CITY.de) and 
sign in with the keyrock admin account (sign out before, if you are already logged in).
2.  Select the entry Applications on the left.

You are presented with the List of configured Applications

3. Select the API Catalogue

Here we can promote a users access rights.

4. In the middle, you can find the field **Authorized users**; Click the `Authorize` Button to the right of that.
5. In the new window type the username of **USER1** in the left field and hit the `+`-Button next to the username.

The user is moved to the list of Authorized users, but has no rights right now.

6. Select the dropdown Menu next to the user in the list to the right and select all roles from the appearing list.
7. Click the `Save` Button

> **_NOTE:_** Since we use the user to test a bunch of features, we select all roles. In a real scenario you would 
give the tenant-admin rights to the user only if this user needs to create a tenant (FIWARE-service) for the platform 
as well as select the suited rights to provide/consume data.

8. Log out by clicking on the admin user name in the top right corner and select `Sign out`.

# 3. Tenants

## 3.1 Tenant creation
In order to send or receive data to/from the Orion Context Broker, we have to specify a so called FIWARE-service to address
a request. Since we gave **USER1** the tenant-admin role this can now be done.

1. Go to [https://apis.fiware.opendata-CITY.de/sign-in](https://apis.fiware.opendata-CITY.de/sign-in), 
but do not enter your credentials.

> **_NOTE:_** The API Platform comes with multiple login methods, but since we use Keyrock for accessing the system 
components, the build-in login to the platform will not work for the users we created in Step 2.1!

2. Click on the `Sign in with Fiware`-Button, fill in the user credentials of **USER1** in the prompted window and click `Sign In`.

<div align="center">![img](platform-testing/img/FiwareSignIn.png)</div>

3. On the Top click on `Tenants`

In order to see a tenant, the person who created the tenant has to add you to that tenant, this is why you probably won't see any tenants.

4. Click on the green `Add Tenant` Button on the right.

The AEMET Data Harvester collecting Data from spanish weather stations, is preconfigured with the FIWARE-service: **aemet**.

5. Set the Tenants name to be **aemet**

> **_NOTE:_** Only lowercase letters and numbers are allowed for the tenant name.

6. Enter any description (apart from leaving empty) in the tenant description field.

7. Click on the green `Add Tenant` Button in the bottom  right.

# 3.2. Tenant functionality
Since you already created a tenant by now, we check for basic functionality of the tenant-manager.
## Test 1
1. Click `Add Tenant`
2. Enter a valid tenant name and a description
3. Click `Add Tenant` to save.
* Expected: Tenant was created without errors and is visible in the list of tenants.

## Test 2
1. Click on `Edit` of the tenant entry, created in Test 1
2. Add something of your choice to the tenant description
3. Add **USER2** to the tenant, by clicking the username in the list, and select `Add User For Tenant`
4. Click `Modify Tenant` to save.
* Expected: Tenant is saved correctly without errors, description is changed and the selected user is shown on the right of the entry.

> **_NOTE:_** Adding the admin user to the tenant may lead to unwanted side-effects.

## Test 3
1. Click on `Remove` of the tenant entry, created in Test 1
2. You are prompted with a confirmation window. Select `Yes, Remove Tenant`
* Expected: Tenant is removed from the list without errors.

## Test 4
1. Switch to the `Authorization` Tab, shown in the top right.
2. Enter your password and select `Get New Token`.
* Expected: An Oauth2 bearer token is presented without error, like:
  ```
  { "access_token": "4a21eee8d657453befcedf983c73b7a6fa556tyh", "token_type": "Bearer", "expires_in": 3600, "refresh_token": "b7fbdkgb72e3c58dndab5fcc923cf9cb4a772cia", "scope": [ "bearer" ] }
  ```


# 4. Keyrock
We tried to get a bearer token in Test 4. This can also be done, by accessing the OAuth2 endpoint of Keyrock.
We will now test for this endpoint by retrieving a token and refreshing it using the refresh token afterwards.

Note: We can't just send a request and will be granted with a token. Some additional information has to be sent towards Keyrock, 
namely Basic Authentication Information for the API Catalogue and an information about the user who wants to get the token.

> **_NOTE:_** The first part of the following steps require an authorized permission level on the API Catalogue, this can be 
done with user created in 1.1 / 1.2 and with the admin account as well.

## 4.1 Get a bearer token

1. Go to [https://accounts.fiware.opendata-CITY.de](https://accounts.fiware.opendata-CITY.de).
2. Sign in with **USER1**, if you are not already signed in.
3. On the left select Applications and then select the API Catalogue from the List.
4. You should find an entry **OAuth2 Credentials**. Click the dropdown icon on the right of it.

<div align="center">![img](platform-testing/img/API_Catalogue.png)</div>

5. Copy both Client ID and Client Secret to an editor. We'll need them later on.
6. Switch to Postman and open the Request "Documentation -> Keyrock -> POST: Get Authtoken"
7. Go to the Authorization Tab, copy the Client ID to **Username** and Client Secret to **Password**

> **_NOTE:_** ClientID and Secret will not be transmitted in plain text.

8. Next go to the Body Tab and insert username and password of **USER1**.

> **_NOTE:_** Although the requests ask for the username, only the email will work.

9. Press `Send`, to send the request to Keyrock.
* Expected: The response's body should look like this:
```
{
    "access_token": "734c1731318e8ab30ccba431b31faaf4a20c4bb9",
    "token_type": "Bearer",
    "expires_in": 3599,
    "refresh_token": "5a054b2d1814f27aa4d3d248f28335e532719149",
    "scope": [
        "bearer"
    ]
}
```

## 4.2 Refresh your token
With every request of a bearer token, you get the bearer token (field: access_token) and a second token
(field: refresh_token), that can be used to refresh your access token to extend the time, since the bearer token will
expire eventually (given in seconds in field: expires_in). 

To refresh the token, we just need to send the refresh token together with the Basic Authentication to Keyrock.
1. In Postman open the Request "Documentation -> Keyrock -> POST: Refresh Token"
2. Again, go to the Authorization Tab, copy the Client ID to **Username** and Client Secret to **Password**
3. Go to the Body tab, copy the refresh_token from the previous step and insert it to the key `refresh_token`
4. Press `Send`, to send the request to Keyrock.
* Expected: The response's body should look like this:

```
{
    "access_token": "efb0dc68f8a04c1d51891be1b95492a41c075fc9",
    "token_type": "Bearer",
    "expires_in": 3599,
    "refresh_token": "8902cac3c2b52619cbc95d8b03384ae0f96f89c4",
    "scope": [
        "bearer"
    ]
}
```

# 5 Manual Data commit

In this section, we will create a tenant, feed it with some data, add a subscription to it
and collect historical data.

## 5.1 Basic Tenant Interaction

1. Go to [https://apis.fiware.opendata-CITY.de](https://apis.fiware.opendata-CITY.de)
and if not already logged in, login with Fiware, using the **USER1**
2. Click `Add Tenant`
3. Enter the tenant name **testtest111** and a description
4. Click `Add Tenant` to save.

The tenant should be created with no errors and be listed.

5. While we are here, Switch to the `Authorization` Tab, enter your password and select `Get New Token`.
6. Copy the access token.
7. Switch to Postman and open the Request "Documentation -> Orion -> GET: TestEntities"
8. In the Headers Tab, insert your bearer token.
9. Press `Send`, to send the request to Orion Context Broker.
* Expected: The response status code should be `200 OK` and the body should be empty: 
```
[]
```

## 5.2 Basic Data Handling

Now we post some data towards the Orion Context Broker.

1. In Postman open the Request "Documentation -> Orion -> POST: TestData"
2. In the Headers Tab, insert your bearer token.

You can take a look at the body. There is the data, that will be sent. You can do changes on the values, if wanted.

3. Press `Send`, to send the request to orion.
* Expected: The response status code should be `204 No Content` and should have no body.

Now we try to read that data from context broker.

4. In Postman open the Request "Documentation -> Orion -> GET: TestEntities"

 > **_NOTE:_** Yes, it is the same as before in Step 5.1, so you could just resend that one, if you did not close the tab.

5. In the Headers Tab, insert your bearer token.
6. Press `Send`, to send the request to orion.
* Expected: The response status code should be `200 OK` and the body should be like: 
```
[
    {
        "id": "first",
        "type": "Thing",
        "precipitation": {
            "type": "Number",
            "value": 0,
            "metadata": {}
        },
        "relativeHumidity": {
            "type": "Number",
            "value": 122,
            "metadata": {}
        }
    }
]
```

## 5.3 Historical Data Handling

Now we create a subscription, send some data and try to access the historical data.

1. In Postman open the Request "Documentation -> Orion -> POST: TestSubscribe"
2. In the Headers Tab, insert your bearer token.

Again, you may take a look at the body, to see the content of the subscription.

3. Press `Send`, to send the request to orion.
* Expected: The response status code should be `201 Created` and should have no body.

Now we just send some data towards the context brocker. 

> **_NOTE:_** Data commited to the context broker now is transferred to QuantumLeap due to the subscribtion we created.

4. In Postman open the Request "Documentation -> Orion -> POST: TestData"

> **_NOTE:_** Yes, it is the same as before in Step 5.2, so if you did not close the tab, you dont need the next step.

5. In the Headers Tab, insert your bearer token.
6. Go to the Body Tab and change the relativeHumidity.
7. Press `Send`, to send the request to orion.
8. Go to the Body Tab and change the relativeHumidity again.
9. Press `Send`, to send the request to orion again.

Now we have to do a small digression for QuantumLeap, since the endpoint for retrieving data is not configured.

10. Go on with **Section 9. Historical Data Acquisition (QuantumLeap)**

# 6 Connecting data from a collection service

As stated before, we will use the harvester in order to feed the system with data.

## 6.1 Create a secret
First we need to create a secret so the system does not reject the deployment.

Connect to the Kubernetes Cluster using Putty and use the following command to create the secret:
```
kubectl create \
    -n fiware-prod \
    secret docker-registry gitlab-data-models-registry \
    --docker-server=gitlab.com/zentrale-open-data-plattform-paderborn:5050 \
    --docker-username=Anton.Hannemann_at_omp.de \
    --docker-password=weCrKsbJVCWzxxwV-FxT
```

> **_NOTE:_**  Currently we use a personalized account, responsibilites and accounts have to be clarified.

## 6.2 Build and deploy
There are 2 ways to deploy harvester to the production system.

**_Option A_**: By Branch creation (the intended way)

The system is setup to make transitions from Development to Staging to Production. So to do a deployment
on production we want to create a Release-Branch based on its Staging branch variant from project [https://gitlab.com/zentrale-open-data-plattform-paderborn/Anton.Hannemann_at_omp.de/data-models/-/branches](https://gitlab.com/zentrale-open-data-plattform-paderborn/Anton.Hannemann_at_omp.de/data-models/-/branches).

For more information on this, please refer to [platform-setup document, Chapter 2](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/tree/master/platform-setup#2-deploy-the-components).

**_Option B_**: Re-deploy (the "but there was no change, just redeploy please" way)

1. Go to [https://gitlab.com/zentrale-open-data-plattform-paderborn/Anton.Hannemann_at_omp.de/data-models/pipelines](https://gitlab.com/zentrale-open-data-plattform-paderborn/Anton.Hannemann_at_omp.de/data-models/pipelines)
and search for the most recent pipeline on any production (R*) branch.
2. Click on the the play button to the right of that pipeline and select `do deploy prod`

# 7. Subscriptions

Data committed to the system from the AEMET Data Harvester is using the FIWARE data-model _WeatherObserved_
and we want the context broker to create a subscription for QuantumLeap so historical data of that data-model
will be made available.

1. Switch to Postman and open the Request "Documentation -> Orion -> POST: AEMET Subscribe"
2. Get a bearer token via Postman, or via APInf and in the header tab paste the token where stated.

You may take a look at the body and see the content of the subscription.

3. Press `Send`, to send the request to orion.
* Expected: The response's body should be empty and result in a `201 Created` should be returned.

**Now let the system collect some data. Please stop here and come back tomorrow.**

> **_NOTE:_**  At this point, the access token might be invalid. Please refer to chapter 4.2 on how to get a new access token.

4. In Postman open the Request "Documentation -> Orion -> GET: AEMET Subscription"
5. Again, paste the bearer token to the stated field in the header tab
6. Press `Send`, to send the request to orion.
* Expected: The response body contains the subscription similar to this:

```
[
    {
        "id": "5eb315de96c6693cd0c33af8",
        "description": "aemet sub",
        "status": "failed",
        "subject": {
            "entities": [
                {
                    "idPattern": ".*"
                }
            ],
            "condition": {
                "attrs": []
            }
        },
        "notification": {
            "timesSent": 2031,
            "lastNotification": "2020-05-07T04:10:53.00Z",
            "attrs": [],
            "onlyChangedAttrs": false,
            "attrsFormat": "normalized",
            "http": {
                "url": "http://quantumleap.fiware-prod.svc.cluster.local:8668/v2/notify"
            },
            "metadata": [
                "dateCreated",
                "dateModified",
                "timestamp"
            ],
            "lastFailure": "2020-05-07T04:10:58.00Z",
            "lastFailureReason": "Timeout was reached",
            "lastSuccess": "2020-05-07T04:10:55.00Z",
            "lastSuccessCode": 200
        }
    },
]
```

> **_NOTE:_** There may be no _lastSuccess_ shown in the result. If so, probable only a _lastFailure_ 
with "Timeout was reached" will be presented. Once data will be sent towards the platform, the 
subscription will successfully handle data, and will show success.

# 8. Current Data Acquisition (Orion Context Broker)

If you just setup the deployment for the harvester, there may be no data. 

In the case of the AEMET Data Harvester, the data is scheduled to be collected once every hour+10 (7:10 / 8:10 / 9:10 ...).

If that may be the case, requests sent during this section will return with Status code `200` but an empty body:

```
[]
```
and only the first of the following Data Acquisitions will be possible.

## Data Acquisition 1 (Unfiltered Data)
1. Switch to Postman and open the Request "Documentation -> Orion -> GET: AEMET Entities"
2. In the Headers Tab, insert your bearer token
3. Press `Send`, to send the request to orion.

In the response body you will see a list of up to 20 entities (limited by orion).

Pick one entity and copy the identifier next to the field **id**.

## Data Acquisition 2 (Entity Data)
1. In Postman open the Request "Documentation -> Orion -> GET: AEMET Station"
2. In the URL at the top you paste the identifier from the previous step where stated.
3. 2. In the Headers Tab, insert your bearer token
4. Press `Send`, to send the request to orion.

In the response body you will see nearly the same output as before, but only of the specified entity.

Now go ahead and copy any attribute name, like **temperature**.

## Data Acquisition 3 (Attribute Data of a specific entity)
1. In Postman open the Request "Documentation -> Orion -> GET: AEMET Attribute"
2. In the Headers Tab, insert your bearer token
3. In addition to the entity of the previous, paste the attribute name where stated.
4. Press `Send`, to send the request to orion.

In the response body only the specific attribute of the specified entity will be shown.

# 9. Historical Data Acquisition (QuantumLeap)

Now we need to add an endpoint for the a FIWARE-service in order to allow data-retrieval.

> **_NOTE:_** As stated in the Sections before, the system needs some time to collect data. Even, if the context broker shows
some data, the harvester needs sufficient time in order to update historical data.

1. Go to [https://umbrella.fiware.opendata-CITY.de/admin/](https://umbrella.fiware.opendata-CITY.de/admin/)
and login with the umbrella admin credentials.
2. At the top click on `Configuration` and select `API Backends`.
3. Select the entry of Orion Context Broker.
4. Click on `Sub-URL Request Settings` near the bottom.

<div align="center">![img](platform-testing/img/SubURLRequestSettings.png)</div>

Now we need to locate the correct sub-url setting as a reference. With respect to this document, you either want to find 
the setting for **aemet** or **testtest111**. Interpret the following images accordingly as they only state **testtest111**.

5. Click on `Edit` of every Setting with **GET** as HTTP Method and `^/` as URL Matcher

<div align="center">![img](platform-testing/img/editSubURL.png)</div>

6. Now either copy all of the following marked entry, or keep the tab open and open an new tab and go to the umbrella website again.

<div align="center">![img](platform-testing/img/GETSubURL.png)</div>

7. If you copied the entries, click cancel. Go on otherwise.
8. At the top click on `Configuration` and select `API Backends`.
9. Select the entry of QuantumLeap
10. Click on `Sub-URL Request Settings` near the bottom.
11. Click on `Add URL Settings`
12. Transfer the fields of the previous image accordingly to this window and click `OK`.

<div align="center">![img](platform-testing/img/addSubURL.png)</div>

13. Click on `Save`.
14. At the top click on `Configuration` and select `Publish Changes`.
15. Make sure, the change for QuantumLeap is selected and press `Publish`.

Done with Umbrella

16. Switch to Postman and open the Request "Documentation -> QuantumLeap -> QuantumLeap-2-1"
17. In the Headers Tab, insert your bearer token

Perform the Steps 18 to 20 only if you have configured the Sub-URLs for aemet.

18. Replace the FIWARE-service **testtest111** with **aemet**.
19. Perform the Request "Documentation -> Orion -> GET: AEMET Entities" and copy one id of your choice
20. Go back to the QuantumLeap-2-1 Tab and in the Request-URL replace **first** with the id you copied.


21. Press `Send`, to send the request to QuantumLeap.

* Expected 200 OK and the historical data of your service

**CONTINUE:** If you have done this chapter as part of section 5.3 AND you choose to include real 
world data, go on with chapter 6. Go on with Chapter 10. Grafana otherwise.


# 10. Grafana

1. Go to [https://grafana.fiware.opendata-CITY.de/login](https://grafana.fiware.opendata-CITY.de/login)
and login with Grafana admin credentials.
>**_NOTE:_** The default username for the admin-account is 'admin'.

2. In the Home Dasboard add a data source by clicking on the Step in the suggested workflow and
recreate the following template (Select PostgreSQL)


<div align="center">![img](platform-testing/img/grafana1.png)</div>

3. Save the Data Source and go back to the Home Dashboard.
4. Select `New Dashboard` in the suggested workflow and select `Add Query`.
5. Search for the blue circle in the next image on the right and select the little pen-icon to go to query mode.

<div align="center">![img](platform-testing/img/grafana2.png)</div>

3. Insert the following example query. 

>**_NOTE:_** The FROM is constructed with: mt+<tenantname>.etthing

```
SELECT
  time_index AS "time",
  relativeHumidity
FROM mttesttest111.etthing
ORDER BY 1
```
>**_NOTE:_** In order for the changes on the query to take effect, please click somewhere outside the editor.

>**_NOTE #2:_** There may be too much time past since the last data update. If the dashboard says "No data", 
check the top right of the dashboard and set the time from "Last 6 hours" to something more suiting... "Last 2 days" or so.

* Expected: All created ok and values are shown.




## 11. Technical notes

If the kubernetes cluster has some performance issues (especially with low disk IOPs) there may be impact on QuantumLeap. One typical symptom is occasionally slow Volume creation. This affects QuantumLeap and QuantumLeap Crate. There is a known problem where QuantumLeap Crate can go into "limp" mode, where Crate does not acknowledge the received data and QuantumLeap thinks that QL Crate is down. However, data is persisted in the Crate DB. This problem will manifest when creating subscriptions. When you GET a subscription, the "lastFailureReason" is "Timeout was reached", and "status" is not active.

Status can be remedied by repeatedly POSTing data (once / minute over 5 minute period).

*Additional information in Grafana queries*

>**_NOTE1:_** It may be that to create a query, you need to access CrateDB to see how it is structured. 
In order to do this, you need to access QuantumLeapCrate container and start Crash CLI tool. In the tool:

```
[root@7be30fcde36d data]# crash
CONNECT OK
cr> show tables;
+--------------------------------------+
| tablename                           |
+--------------------------------------+
| etheaterenergyconsumptionmeasurement |
| mdets_metadata                      |
+--------------------------------------+
SHOW 2 rows in set (0.160 sec)
cr>
```

where:

```
cr> select * from mdetsmetadata;
```
would yield data base content. From starting here, you can search FROM part to the query. For example:

```
"mttemperature"."etairtemp"                                   |
| {"entityid": ["id", "Text"], "entitytype": ["type", "Text"], "humidity": ["humidity", "Number"], "pressure": ["pressure", "hpa"], "temperature": ["temperature", "Number"], "timeindex": ["timeindex", "DateTime"], "timeinstant": ["TimeInstant", "ISO8601"]}
```

FROM then being "mttemperature"."etairtemp"

>**_NOTE2_**:  We can also see that time is "time_index" and there is data for humidity, pressure and temperature

Typing \\q + Enter will exit Crash:
```
cr> \q
Bye!
[root@7088a1a03328 data]#
```



