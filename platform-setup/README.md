# Platform Setup
This document describes the process of setting up the components needed for the complete Software stack (FIWARE and API-Management), the initial configurations and also tests for checking the correct installation of the components and also tests to check if the software is operational. In sum, these tests can be used as acceptance tests for the installation. 

> **_NOTE:_**  The description of the services is out of scope of this documentation. While this document is intended to enable you to run the setup and configuration w/o an in deep knowledge of the Services, it's still necessary to read the services' documentation in order to get that knowledge about how to use them.


## Prerequisites for the deployment:

* Kubernetes cluster is setup with a namespace called fiware-prod (assuming master on machine 'Kube02').
* A User is provided with a _kube.config_ file to access and control the cluster in previously mentioned namespace.<br>This file contains credentials to the cluster, like an access key. The file is assumed to be found in the '/opt/testing/' directory.
* Certificate files need to be present on the machine that is used to create certificate secrets.<br>The files are assumed to be found in the '/etc/letsencrypt/live/fiware.opendata-CITY.de/' directory.  
* Each component project is equipped with the required Gitlab-variables. Required Gitlab-variables are listed in the component project readme.md file.  

## What you need

* An ssh-client (e.g. Putty) (https://www.putty.org/) and an appropriate user to access the cluster.<br>Used to run commands on the cluster.
* A browser (tested with Chrome [https://www.google.com/intl/de_de/chrome/])
* optional: Postman (https://www.postman.com/downloads/)<br>For your convenience, Postman-Collections are provided for testing the installation/configuration. You can run _curl_ commands alternatively.
* Access to your local Gitlab (granted access to create branches from repositories).
* You'll need 3 different E-Mail-Accounts by hand (One for Keyrock, one for APInf and another one for testing)

## 1. Create Kubernetes secrets

> **_NOTE:_**  Kubernetes secrets are used until Vault integration is done.

### 1.1 Create Secrets from the certificates

#### NOTE: Umbrella certificates are expected to be handled by Kubernetes provider.
If you are re-deploying the system and you have not deleted Umbrella certificate secrets, you can skip this step.

Example with Let's Encrypt certificates.
Within your ssh-client run the following commands:
```
sudo kubectl create secret generic umb-privkey --from-file=/etc/letsencrypt/live/fiware.opendata-CITY.de/privkey.pem -n fiware-prod --kubeconfig /opt/testing/prodkube.config

sudo kubectl create secret generic umb-fullchain --from-file=/etc/letsencrypt/live/fiware.opendata-CITY.de/fullchain.pem -n fiware-prod --kubeconfig /opt/testing/prodkube.config
```
### 1.2 Create Keyrock Default Admin Credentials
> **_NOTE:_** Change **EMAIL** and **YOURPASSWORD** to your liking.
```
sudo kubectl create secret generic idm-secret --from-literal=email=EMAIL --from-literal=password=YOURPASSWORD -n fiware-prod --kubeconfig /opt/testing/prodkube.config
```
### 1.3 Create MaxMind License
The MaxMind GeoLite2 license is needed. Please visit [MaxMind](https://www.maxmind.com/en/geoip2-services-and-databases) website to get more information about the license.  
```
sudo kubectl create secret generic maxmind-license --from-literal=license=XXX -n fiware-prod --kubeconfig /opt/testing/prodkube.config
```
### 1.4 Create EMail Credentials
SMTP Email credentials.
```
sudo kubectl create secret generic email-secret --from-literal=host=SMTP_HOST --from-literal=user=EMAIL_ADDRESS --from-literal=password=XXX -n fiware-prod --kubeconfig /opt/testing/prodkube.config
```
### 1.5 Create Grafana Admin Credentials 
Grafana password for the user 'admin'.
> **_NOTE:_** Change **YOURPASSWORD** to your liking.
```
sudo kubectl create secret generic grafana-password --from-literal=password=YOURPASSWORD -n fiware-prod --kubeconfig /opt/testing/prodkube.config
```

### 1.6 Create Perseo secret 
Following long command creates a file that we will use to create Perseo secret. Keyrock admin email and password are needed for basic operation. See section 'config.authentication'. Please change KEYROCK_ADMIN_EMAIL and KEYROCK_ADMIN_PASSWORD to your needs.
```
sudo cat <<EOF >config.js
'use strict';

var config = {};

config.logLevel = 'INFO';

config.endpoint = {
    host: 'localhost',
    port: 9090,
    rulesPath: '/rules',
    actionsPath: '/actions/do',
    noticesPath: '/notices',
    vrPath: '/m2m/vrules',
    checkPath: '/check',
    versionPath: '/version',
    logPath: '/admin/log',
    metricsPath: '/admin/metrics'
};

config.isMaster = true;

config.slaveDelay = 500;

config.mongo = {
    // The URI to use for the database connection. It supports replica set URIs.
    // mongodb://[username:password@]host1[:port1][,host2[:port2],...[,hostN[:portN]]][/[database][?options]]
    // I.e.: 'mongodb://user:pass@host1:27017,host2:27018,host3:27019/cep?replicaSet=myrep'
    url: 'mongodb://localhost:27017/cep'
};

config.orionDb = {
    url: 'mongodb://localhost:27017/orion',
    collection: 'entities',
    prefix: 'orion',
    batchSize: 500
};

config.perseoCore = {
    rulesURL: 'http://localhost:8080/perseo-core/rules',
    noticesURL: 'http://localhost:8080/perseo-core/events',
    interval: 60e3 * 5
};

config.nextCore = {
};

config.smtp = {
    port: 25,
    host: 'host',
    secure: false,
    auth: {
     user: 'email',
     pass: 'password'
     }
     ,
    tls: {
        rejectUnauthorized: false
    }
};

config.sms = {
    URL: 'http://sms-endpoint/smsoutbound',
    API_KEY: '',
    API_SECRET: '',
    from: 'tel:22012;phone-context=+34'
};

config.smpp = {
    host: '',
    port: '',
    systemid: '',
    password: '',
    from: '346666666',
    enabled: false
};

config.orion = {
    URL: 'http://orion-endpoint:1026'
};

config.pep = {
    URL: 'http://pep-endpoint:1026'
};

config.authentication = {
    host: 'keyrock',
    port: '3000',
    user: 'KEYROCK_ADMIN_EMAIL',
    password: 'KEYROCK_ADMIN_PASSWORD'
};

config.collections = {
    rules: 'rules',
    executions: 'executions'
};

config.executionsTTL = 1 * 24 * 60 * 60;

config.DEFAULT_SUBSERVICE = '/';
config.DEFAULT_SERVICE = 'unknownt';

config.checkDB = {
    delay: 5e3,
    reconnectTries: 1e3,
    reconnectInterval: 5e3,
    bufferMaxEntries: 5
};

config.restBase = null;

config.castTypes = false;

module.exports = config;

EOF
```

Create the secret with the file we just created:
```
sudo kubectl create secret generic perseo-config --from-file=config.js -n fiware-prod --kubeconfig /opt/testing/prodkube.config
```
For security reasons, clean up, by removing previously made file:
```
sudo rm config.js
```


## 2. Deploy the Components
The components are deployed to the production cluster by creating a release branch (starting with a capital 'R' e.g. "Release 1.0") from the existing Release Candidate-branch (starting with a capital 'C'). The creation of the branch will start the branches pipeline immediately. For safety reasons, the deploy job is set manually on Release-branches so you have to start the deployment by clicking the appropriate button on that job.
> **_NOTE:_** Some Gitlab-Projects have only ONE job (deploy). Those pipelines are marked "skipped" right after creation of the branch and will wait for you to start the job manually while other projects have a build-job, that runs automatically. Wait for this job to end "sucessfully" before starting the deployment manually.
 
For more information on Gitlab branch strategy and project structure, see the Wiki on Gitlab.com.

Deploy all components in following order:
 * Mongo DB (https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/mongo)
 * Orion (https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/orion)
 * Umbrella (https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/umbrella)
 * Keyrock (https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/keyrock)
 * APInf Platform (https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/platform)
 * QuantumLeap (https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/quantumleap)
 * Grafana (https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/grafana)
 * Kurento (https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/kurento)
 * Perseo (https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/perseo)
 * Cosmos (https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/cosmos)

## 3. Test Correct Installation of the Components

### 3.1 Run the Test Script
The script tests the connectivity from Platform to each component, by checking open port from each component.
```
sudo kubectl -n fiware-prod --kubeconfig /opt/testing/prodkube.config exec -it $(kubectl get pod -l app=apinf -o jsonpath="{.items[0].metadata.name}" -n fiware-prod --kubeconfig /opt/testing/prodkube.config) -- bash -c "apt update; apt install netcat -y; echo; echo  TESTING; timeout 4 nc mongo 27017 -vw2; timeout 4 nc orion 1026 -vw2; timeout 4 nc umbrella 443 -vw2; timeout 4 nc umbrella-elasticsearch 9200 -vw2; timeout 4 nc quantumleap 8668 -vw2; timeout 4 nc quantumleapcrate 4200 -vw2; timeout 4 nc keyrock 3000 -vw2; timeout 4 nc grafana 3000 -vw2; timeout 4 nc kurento 8888 -vw2; timeout 4 nc perseo-core 8081 -vw2; timeout 4 nc perseo-fe 9090 -vw2; timeout 4 nc flink-jobmanager 8081 -vw2; echo END TEST"
```
The output will be similar to this:
```
Hit:1 http://security.debian.org/debian-security stretch/updates InRelease
Ign:2 http://deb.debian.org/debian stretch InRelease
Hit:3 http://deb.debian.org/debian stretch-updates InRelease
Hit:4 http://deb.debian.org/debian stretch Release
Reading package lists... Done
Building dependency tree
Reading state information... Done
16 packages can be upgraded. Run 'apt list --upgradable' to see them.
Reading package lists... Done
Building dependency tree
Reading state information... Done
netcat is already the newest version (1.10-41).
0 upgraded, 0 newly installed, 0 to remove and 16 not upgraded.

TESTING
mongo.fiware-prod.svc.cluster.local [192.168.179.49] 27017 (?) open

orion.fiware-prod.svc.cluster.local [192.168.208.246] 1026 (?) open

umbrella.fiware-prod.svc.cluster.local [192.168.174.152] 443 (?) open

umbrella-elasticsearch.fiware-prod.svc.cluster.local [192.168.155.252] 9200 (?) open

quantumleap.fiware-prod.svc.cluster.local [192.168.209.209] 8668 (?) open

quantumleapcrate.fiware-prod.svc.cluster.local [192.168.210.62] 4200 (?) open

keyrock.fiware-prod.svc.cluster.local [192.168.151.137] 3000 (?) open

grafana.fiware-prod.svc.cluster.local [192.168.164.156] 3000 (?) open

kurento.fiware-prod.svc.cluster.local [192.168.242.209] 8888 (?) open

perseo-core.fiware-prod.svc.cluster.local [192.168.160.65] 8081 (?) open

perseo-fe.fiware-prod.svc.cluster.local [192.168.175.45] 9090 (?) open

flink-jobmanager.fiware-prod.svc.cluster.local [192.168.174.247] 8081 (?) open

END TEST
```
Please check, that every Port is in 'Open' state.
> **_NOTE:_** It may take some time until every pod is up and running. If one or more pods' port is not in open state, please re-execute the command after waiting for about 5 minutes. 
### 3.2 Further Tests on the Components
Beyond having all Pods up and running and having the services listening on a specific port, some more tests on each component will take place here.
#### 3.2.1 Mongo DB
The following command will execute the Mongo DB client and try to connect to the running mongo instance:
```console
sudo kubectl -n fiware-prod --kubeconfig /opt/testing/prodkube.config exec -it $(kubectl get pod -l app=mongo -o jsonpath="{.items[0].metadata.name}" -n fiware-prod --kubeconfig /opt/testing/prodkube.config) -- timeout 2 mongo
```
The output will look like:
```console
MongoDB shell version v3.6.17
connecting to: mongodb://127.0.0.1:27017/?gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("264b065e-b8b8-4cf7-848c-843de314391a") }
MongoDB server version: 3.6.17
Server has startup warnings: 
2020-05-04T08:29:42.261+0000 I STORAGE  [initandlisten] 
2020-05-04T08:29:42.261+0000 I STORAGE  [initandlisten] ** WARNING: Using the XFS filesystem is strongly recommended with the WiredTiger storage engine
2020-05-04T08:29:42.261+0000 I STORAGE  [initandlisten] **          See http://dochub.mongodb.org/core/prodnotes-filesystem
2020-05-04T08:30:29.765+0000 I CONTROL  [initandlisten] 
2020-05-04T08:30:29.765+0000 I CONTROL  [initandlisten] ** WARNING: Access control is not enabled for the database.
2020-05-04T08:30:29.765+0000 I CONTROL  [initandlisten] **          Read and write access to data and configuration is unrestricted.
2020-05-04T08:30:29.765+0000 I CONTROL  [initandlisten]  
> command terminated with exit code 124
```
Please check for any errors.
#### 3.2.2 Orion
The following command will execute the context broker client and retrieve the context broker's version:
```console
sudo kubectl exec --kubeconfig /opt/testing/prodkube.config -it $(kubectl get pod --kubeconfig /opt/testing/prodkube.config -l app=orion -o jsonpath="{.items[0].metadata.name}" -n fiware-prod) -n fiware-prod -- contextBroker --version
```
Example output:
```console
2.3.0 (git version: 764f44bff1e73f819d4e0ac52e878272c375d322)
Copyright 2013-2019 Telefonica Investigacion y Desarrollo, S.A.U
Orion Context Broker is free software: you can redistribute it and/or
modify it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

Orion Context Broker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
General Public License for more details.

Telefonica I+D
```
Please check for any errors.
#### 3.2.3 Elasticsearch
The following command retrieves the current version of the Elasticsearch Service in json format:
```console
sudo kubectl -n fiware-prod --kubeconfig /opt/testing/prodkube.config exec -it $(kubectl get pod -l app=umbrella-elasticsearch -o jsonpath="{.items[0].metadata.name}" -n fiware-prod --kubeconfig /opt/testing/prodkube.config) -- curl http://127.0.0.1:9200  
```
Example output:
```json
{
  "name" : "Nosferata",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "hYscOzasQF6iC8HKuNxuYw",
  "version" : {
    "number" : "2.4.6",
    "build_hash" : "5376dca9f70f3abef96a77f4bb22720ace8240fd",
    "build_timestamp" : "2017-07-18T12:17:44Z",
    "build_snapshot" : false,
    "lucene_version" : "5.5.4"
  },
  "tagline" : "You Know, for Search"
}
```
Please check for any errors.
#### 3.2.4 Umbrella
The following command checks the Umbrella log for a specific content:
```console
sudo kubectl -n fiware-prod --kubeconfig /opt/testing/prodkube.config logs $(kubectl get pod -l app=umbrella -o jsonpath="{.items[0].metadata.name}" -n fiware-prod --kubeconfig /opt/testing/prodkube.config) | grep "+ api-umbrella"
```
Expected output:
```console
+ api-umbrella run
```
#### 3.2.5 APInf Platform
The following command checks the APInf log for Errors:
```console
sudo kubectl -n fiware-prod --kubeconfig /opt/testing/prodkube.config logs $(kubectl get pod -l app=apinf -o jsonpath="{.items[0].metadata.name}" -n fiware-prod --kubeconfig /opt/testing/prodkube.config) | grep error
```
Example output:
```console
(node:1) UnhandledPromiseRejectionWarning: Unhandled promise rejection. This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). (rejection id: 1)
(node:1) UnhandledPromiseRejectionWarning: Unhandled promise rejection. This error originated either by throwing inside of an async function without a catch block, or by rejecting a promise which was not handled with .catch(). (rejection id: 2)
raven@2.6.4 alert: no DSN provided, error reporting disabled
```
Please check for **severe** errors. (Please note: There may appear errors not really critical)
#### 3.2.6 Crate DB
The following command retrieves the current version of the Crate DB Service in json format:
```console
sudo kubectl -n fiware-prod --kubeconfig /opt/testing/prodkube.config exec -it $(kubectl get pod -l app=quantumleapcrate -o jsonpath="{.items[0].metadata.name}" -n fiware-prod --kubeconfig /opt/testing/prodkube.config) -- curl 127.0.0.1:4200
```
Example output:
```json
{
  "ok" : true,
  "status" : 200,
  "name" : "Ilmspitze",
  "cluster_name" : "democluster",
  "version" : {
    "number" : "3.3.5",
    "build_hash" : "df03f2e679d64ae67aed60394c0da82b41b0c63a",
    "build_timestamp" : "2019-07-08T13:20:14Z",
    "build_snapshot" : false,
    "es_version" : "6.5.1",
    "lucene_version" : "7.5.0"
  }
}
```
#### 3.2.7 Quantumleap
The following command retrieves the current version of the QuantumLeap Service in json format:
```console
sudo kubectl -n fiware-prod --kubeconfig /opt/testing/prodkube.config exec -it $(kubectl get pod -l app=quantumleap -o jsonpath="{.items[0].metadata.name}" -n fiware-prod --kubeconfig /opt/testing/prodkube.config) -- curl http://0.0.0.0:8668/v2/version
```
Example output:
```json
{
  "version": "0.7.5"
}
```
## 4. Configure the Components
### 4.1 Configure Umbrella
API Umbrella works as a proxy for the rest of the services.<br>
All traffic from and to the internet should be routed through Umbrella.  
Umbrella needs DNS setup with certificates for the domain.
For a complete documentation on Umbrella, please refer to https://api-umbrella.readthedocs.io/en/latest/
#### 4.1.1 Create Admin on First Start 
On the first access to https://umbrella.fiware.opendata-CITY.de/admin/ you need to sign-up an admin user.  
After this, you can use this admin user to configure Umbrella.
#### 4.1.2 Create Backends
To start configuration, make sure you are logged into Umbrella admin panel at 'https://umbrella.fiware.opendata-CITY.de/admin/'

At the top, choose: `Configuration` -> `Website Backends`

Choose `+ Add Website Backend`:  
![img](platform-setup/img/umbrella_create_web.png)

Add the following configurations:
```
Frontend Host: accounts.fiware.opendata-CITY.de
Backend Protocol: http
Backend Server: keyrock.fiware-prod.svc.cluster.local
Backend Port: 3000
SAVE

Frontend Host: apis.fiware.opendata-CITY.de
Backend Protocol: http
Backend Server: apinf.fiware-prod.svc.cluster.local
Backend Port: 3333
SAVE

Frontend Host: grafana.fiware.opendata-CITY.de
Backend Protocol: http
Backend Server: grafana.fiware-prod.svc.cluster.local
Backend Port: 3000
SAVE

Frontend Host: knowage.fiware.opendata-CITY.de
Backend Protocol: http
Backend Server: knowage.fiware-prod.svc.cluster.local
Backend Port: 8082
SAVE
```

After changes are made to Umbrella backends, changes need to be published to take effect:

Choose: `Configuration` -> `Publish changes`:  
![img](platform-setup/img/umbrella_publish_menu.png)

Then choose: `Check all` and `Publish` afterwards:  
![img](platform-setup/img/umbrella_publishing.png)

#### 4.1.3 Create API User
While the previously created admin user is used to create backends and do other configurations, an API-user is needed for generating an API-Key that can be used then by other systems.
> **_NOTE:_** It's important to use the same EMail-Adress that is used with the Admin-User before for this API-User!

At the top, choose: `Users` -> `API Users`

then choose `+ Add New API User`:   
![img](platform-setup/img/umbrella_publishing.png)


Fill in the required details:
```
E-mail: **Use same as Admin-User**

First Name: ex. Admin

Last Name: ex. Admin

Purpose: ex. Some purpose

Agree to terms and conditions by making sure `User agrees to the terms and conditions` is checked.

Other settings will be left to default settings.
```
Press the `Save`-Button

Initial Umbrella configuration is now complete. We will return to Umbrella later on. It's a good idea to keep the browser tab open.

### 4.2 Configure Keyrock (IDM)
Keyrock is the FIWARE component responsible for Identity Management (IDM).

For more information on Keyrock, please refer to <https://fiware-idm.readthedocs.io/en/7.8.1/>

Go to following website: <https://accounts.fiware.opendata-CITY.de/>

Deployment of a fresh Keyrock, with a fresh database, creates an Admin user.<br>
> **_NOTE:_** The credentials for that Admin user were created as Kubernetes secrets, before component is deployed (see chapter 1.2).

> **_NOTE #2:_** This password is also used by Tenant-manager! If this password is changed, some actions are needed to keep tenant-manager operational. Follow instructions here:<br>
> 1) Delete old Tenantmanager-credentials secret with following command:<br>
> `kubectl -n fiware-prod --kubeconfig /opt/testing/prodkube.config delete secret tenantmanager-credentials`
> 2) Update Credentials.json and create secret as described in Chapter 6.1
> 3) Redeploy Tenant Manager as described in Chapter 6.2

Login with that user now by filling the user-email in (1), the password in (2) and by pressing the Sign In button (3) afterwards.<br>
![img](platform-setup/img/keyrock_login.png)

Once logged in to Keyrock, on home tab we can see our registered Applications and Organisations. By default, none are present.  
In the next chapters we will register some applications. For more information on applications, please refer to <https://fiware-idm.readthedocs.io/en/7.8.1/user_and_programmers_guide/application_guide/index.html>
#### 4.2.1 Register Application "API Catalogue"
Press `Register` on the corner of Applications section.  
![img](platform-setup/img/keayrock_register_application.png)  
This opens the "Application Information" Window:   
![img](platform-setup/img/keyrock_application_information.png)  
Fill in the information:

```
Name: API Catalogue

Description: Catalogue of APIs provided using APInf

Url: https://apis.fiware.opendata-CITY.de/

Callback Url: https://apis.fiware.opendata-CITY.de/_oauth/fiware

Signout URL: https://apis.fiware.opendata-CITY.de/
```

Click the `Next`-Button two times

Manage Roles:

Press Roles `+`-Sign to add new role:
```
tenant-admin
```
Click the `Save`-Button.

Repeat the above for two more roles:
```
data-provider
AND
data-consumer
```

Press the `Save`-Button

In this **application page** of API Catalogue, click on the drop down of settings called `OAuth2 Credentials`. This reveals Applications Client ID, Client Secret and Token types.  
![img](platform-setup/img/keyrock_get_oauth.png)  

> **_NOTE:_** We will need these credentials later in the configuration. So for your convinience, you may want to save ClientID and Client Secret in a text editor somewhere.

**Select Token types:**

By default, no token types are selected. To select Token types click on the drop down menu to see your options and then click on the types you want to enable. A check mark will appear on checked items and they will show under the Token types title.

Activate token types: Json Web Token, Permanent

Switch to **Authorized users** section in the application

In section **Authorized users** click `Authorize`

![img](platform-setup/img/keyrock2.1.png)

Give more roles to authorized user Admin: click the `roles` dropdown-menu and click to enable more roles.

```
admin - Click to select ALL roles
SAVE
```

You can now return to home tab, by clicking `home`-icon on the left panel.

With help of above example we can now also setup required Market application.

#### 4.2.2 Register Application "Market"
Press `Register` on the corner of Applications section.

Fill in the information:

```
Name: Market

Description: Market service provided by the Business API Ecosystem

URL: https://market.fiware.opendata-CITY.de

Callback URL: https://market.fiware.opendata-CITY.de/auth/fiware/callback

Click: NEXT - NEXT

Add Roles: seller, customer, orgAdmin, admin
SAVE

Authorized Users: admin - Click to select ALL roles
SAVE
```

You can now return to home tab, by clicking `home`-icon on the left panel.

**Configure "Trusted applications" section in each of the previously made applications**

In `Applications` section, you can click your application to change configurations.

In API Catalogue application settings, press Trusted applications '+ Add'

Find Market using Applications filter: type in 'market' and press '+'.

Press: **SAVE**

In Market application settings, press Trusted applications '+ Add'

Find API Catalogue using Applications filter: type in 'api' and press '+'.

Press: **SAVE**

#### 4.2.3 Register Application "Knowage"
Press `Register` on the corner of Applications section.

Please note that you need two users for Knowage. Create another user for the login. In the instructions we refer to this user as "knowageUser".

Fill in the information:

```
Name: Knowage

Description: Knowage integration

URL: https://knowage.fiware.opendata-CITY.de/knowage/servlet/AdapterHTTP?PAGE=LoginPage

Callback URL: https://knowage.fiware.opendata-CITY.de/knowage/servlet/AdapterHTTP?PAGE=LoginPage

Sign-out Callback Url: https://knowage.fiware.opendata-CITY.de/knowage

Click: NEXT - NEXT

Add Roles: DEV, ADMIN, USER
SAVE

Authorized Users: admin - Click to select ALL roles

Authorized Users: knowageUser - Click to select ALL roles

SAVE
```

You can now return to home tab, by clicking `home`-icon on the left panel.

Keyrock is now configured but we will return to get previously mentioned Client ID and Client secret of API Catalogue. It's a good idea to keep the browser tab open.

### 4.3 Configure APInf Platform

#### 4.3.1 Create Admin on first sign up

Sign up to APInf platform at <https://apis.fiware.opendata-CITY.de> as “Admin”. **If no user is present, first user signed up will be platform admin.** *Note: You can use any username except lowercase 'admin'*

Enter username, email, password and Register

After registration you will be signed in as admin.

#### 4.3.2 Add Proxies

Press Cog wheel on top right and from the menu choose **Proxies**:  
![img](platform-setup/img/apinf_cog.png)  

Choose 'ADD PROXY':  
![img](platform-setup/img/apinf_add_proxy.png)  

**Orion Context Broker**

```
Name: Orion Context Broker

Description: API umbrella installation for the Orion Context Broker service.

Type: apiUmbrella

URL: https://context.fiware.opendata-CITY.de

API Key: <umbrella user API key> - SEE BELOW for instruction

Auth Token: <umbrellaaccounttoken> - SEE BELOW for instruction

ElasticSearch: http://umbrella-elasticsearch.fiware-prod.svc.cluster.local:9200

SAVE
```

Umbrella user API key can be found in Umbrella website under Users > API Users

Click on the User you created earlier and click reveal to show API Key

Umbrella account token can be found in Umbrella website. Top right corner Cog wheel > My account: Admin API Token

Choose 'ADD PROXY'

**QuantumLeap**

```
Name: QuantumLeap

Description: API umbrella installation for the QuantumLeap service.

Type: apiUmbrella

URL: https://sthdata.fiware.opendata-CITY.de

API Key: <umbrella user API key> - SEE BELOW for instruction

Auth Token: <umbrellaaccounttoken> - SEE BELOW for instruction

ElasticSearch: http://umbrella-elasticsearch.fiware-prod.svc.cluster.local:9200

SAVE
```

> **_NOTE:_** Umbrella user API key can be found in Umbrella website under Users > API Users<br>Click on the User you created earlier and click reveal to show API Key<br>Umbrella account token can be found in Umbrella website. Top right corner Cog-wheel > My account: Admin API Token.

#### 4.3.3 Configure Login Platforms
  
In Platform preferences-page select tab **Login Platforms**:  
![img](platform-setup/img/apinf_login_platforms.png)  

Fill in the following information:

Client id: <**client_id**> from “API Catalogue” application in Keyrock. See **Get Client-ID** below.

> **_Get Client-ID:_** Open Keyrock, select application "API Catalogue" and click on the dropdown settings called OAuth2 Credentials. Then copy the Client-ID.

Secret: <**secret**> from “API Catalogue” application in Keyrock. See **Get Client-Secret** below.

> **_Get Client-SECRET:_** Open Keyrock, select application "API Catalogue" and click on the dropdown settings called OAuth2 Credentials. Then copy the secret.

Root url: https://accounts.fiware.opendata-CITY.de

Scroll down and press '**SAVE**'

#### 4.3.4 Configure Settings

In Platform preferences-page select tab **Settings**:  
![img](platform-setup/img/apinf_settings.png)  

Fill in the following information:

Only platform administrators are allowed to add new APIs - **Enabled**

Only platform administrators are allowed to add new Organizations - **Enabled**

Mail – **Enabled**

```
Username: EMAIL_ADDRESS

Password: *********

SMTP Host: SMTP_HOST

SMTP Port: 587

Email for sending mails: EMAIL_ADRESS
```

Disabled login methods. Make sure following are checked:

```
Github
Hsl id
```

Tenant Manager – **Enabled**

```
Url and basepath: https://umbrella.fiware.opendata-CITY.de/tenant-manager/
```

Press **SAVE**

#### 4.3.5 Configure APIs

At the top, select `APIs`:  
![img](platform-setup/img/apinf_apis.png) 

Choose `ADD NEW API`:  
![img](platform-setup/img/apinf_add_api.png) 

##### 4.3.5.1 Configure Orion Context Broker

API Name: Orion Context Broker

Description:

```
Context information provided using the FIWARE Orion Context Broker in right-time.
```

API Host URL:

```
http://orion.fiware-prod.svc.cluster.local
```

Click 'ADD API'

Select tab: **Settings**(General)

```
API visibility: Public
```

Select tab: **Settings**(Network)

```
Proxy: Orion Context Broker

Proxy base path (append to the fixed URL including the leading and trailing slashes): /v2/

API base path (append to the fixed URL including the leading and trailing slashes): /v2/

API Port: 1026

-- Advanced settings:
IDP App Id: <API Catalogue – client_id> - from Keyrock - See "Get Client-ID" below

Rate limit mode: Unlimited requests

SAVE CONFIGURATION
```
> **_Get client-ID:_** In application "API Catalogue" on Keyrock click on the dropdown settings called OAuth2 Credentials, to find API Catalogue Client ID.

Press **SAVE CONFIGURATION**

In the Top select tab: **Endpoints**:  
![img](platform-setup/img/apinf_endpoints.png)

Click 'MANAGE':  
![img](platform-setup/img/apinf_manage_endpoints.png)


> **_NOTE:_** Endpoints BUG, double entry needed! Needs to be done twice to take effect!*

```
Provide API documentation via: URL

Link to API documentation: https://raw.githubusercontent.com/Fiware/specifications/master/OpenAPI/ngsiv2/ngsiv2-openapi.json

Allow “GET” method only
SAVE
```

In the Top select tab: **Monitoring**:  
![img](platform-setup/img/apinf_monitoring.png)

Make sure 'Enable API monitoring' is checked:

```
Endpoint to monitor: :1026/version
```

Press '**SAVE**'

##### 4.3.5.2 Configure QuantumLeap

At the top, select APIs\
Choose 'ADD NEW API'

API Name: QuantumLeap

Description:

```
QuantumLeap is the first implementation of an API that supports the storage of NGSI FIWARE NGSIv2 data into a time-series database.
```

API Host URL:

```
http://quantumleap.fiware-prod.svc.cluster.local
```

Click 'ADD API'

Select tab: **Settings**(General)

```
API visibility: Public
```

Select tab: **Settings**(Network)

```
Proxy: QuantumLeap

Proxy base path: /ql/

API base path: /

API Port: 8668

Advanced settings:
IDP App Id: <API Catalogue – client_id> - From keyrock

Rate limit mode: Unlimited requests

SAVE CONFIGURATION
```

In the top select tab: **Endpoints**

Click 'MANAGE'

*Endpoints BUG, double entry needed. Needs to be done twice to take effect!*

```
Provide API documentation via: URL

Link to API documentation: https://raw.githubusercontent.com/smartsdk/ngsi-timeseries-api/master/specification/quantumleap.yml

Allow “GET” method only
SAVE
```

In the top select tab: **Monitoring**

Make sure 'Enable API monitoring' is checked:

```
Endpoint to monitor: :8668/v2/version
```

Press '**SAVE**'

#### 4.3.6 Configure Branding

Press Cog wheel on top right > **Branding**

Select tab **About**:  
![img](platform-setup/img/apinf_branding_about.png)

```
Site title: YOUR SITE TITLE - ex. Your City Prod
UPDATE
```

Select tab: **Showcase APIs**

```
Showcase APIs: choose “QuantumLeap” AND “Orion Context Broker”
Click on the FEATURED APIs text form and select Qauntum Leap and Orion Context Broker
UPDATE
```

## 5. Install Tenant Manager
### 5.1 Create Secret from Credentials.json
Where to find the needed information:

* **Market-ClientID**: In Keyrock, Application Market, OAuth-Credentials
* **APICatalogue-ClientID**: In Keyrock, Application API Catalogue, OAuth-Credentials
* **KEYROCK-ADMIN-EMAIL**: Created within the secret "idm-secret" in chapter 1.2
* **KEYROCK-ADMIN-PASSWORD**: Created within the secret "idm-secret" in chapter 1.2
* **UMBRELLA-ADMIN-APITOKEN**: Umbrella, on the top right: Settings, My Account 
* **UMBRELLA-API-USER-APIKEY**: Umbrella, Users, API-Users, choose the account created before

First, prepare the Credentials.json with the following command:
```
sudo cat <<EOF >credentials.json
{
    "bae": {
        "client_id": "Market-ClientID"
    },
    "broker": {
        "client_id": "APICatalogue-ClientID"
    },
    "idm": {
        "user_id": "admin",
        "user": "KEYROCK-ADMIN-EMAIL",
        "password": "KEYROCK-ADMIN-PASSWORD"
    },
    "umbrella": {
        "token": "UMBRELLA-ADMIN-APITOKEN",
        "key": "UMBRELLA-API_USER-APIKEY"
    }
}
EOF
```
Create the secret with the file we just created:
```
sudo kubectl create secret generic tenantmanager-credentials --from-file=credentials.json -n fiware-prod --kubeconfig /opt/testing/prodkube.config
```
For security reasons, clean up, by removing previously made file:
```
sudo rm credentials.json
```
### 5.2 Deploy Tenant Manager
As described in chapter 2, deploy Tenant Manager from Gitlab Project https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/tenantmanager.
### 5.3 Test Tenant manager
The following command will show the Tenant Manager's log:
```console
sudo kubectl -n fiware-prod --kubeconfig /opt/testing/prodkube.config logs --tail=10 $(kubectl get pod -l app=tenantmanager -o jsonpath="{.items[0].metadata.name}" -n fiware-prod --kubeconfig /opt/testing/prodkube.config)
```
The output will look like:
```console
[2020-05-04 09:47:27 +0000] [6] [INFO] Starting gunicorn 20.0.4
[2020-05-04 09:47:27 +0000] [6] [INFO] Listening at: http://0.0.0.0:5000 (6)
[2020-05-04 09:47:27 +0000] [6] [INFO] Using worker: sync
[2020-05-04 09:47:27 +0000] [9] [INFO] Booting worker with pid: 9
[2020-05-04 09:47:27 +0000] [10] [INFO] Booting worker with pid: 10
[2020-05-04 09:47:28 +0000] [11] [INFO] Booting worker with pid: 11
[2020-05-04 09:47:28 +0000] [12] [INFO] Booting worker with pid: 12
[2020-05-04 10:23:18 +0000] [10] [INFO] Worker exiting (pid: 10)
[2020-05-04 10:23:18 +0000] [23] [INFO] Booting worker with pid: 23
```
Please check for any errors.

## 6. Configure Umbrella

Visit <https://umbrella.fiware.opendata-CITY.de/admin/>

### 6.1 Add Tenant Manager Backend

Select `Configuration` -> `API Backends` -> `Add API Backend`:  
![img](platform-setup/img/umbrella_add_api_backend.png)

Fill in the information:
```
Name: Tenant Manager

click “Add Server”

Host: tenantmanager.fiware-prod.svc.cluster.local

Port: 5000

Click OK

Frontend Host: umbrella.fiware.opendata-CITY.de

Backend Host: umbrella.fiware.opendata-CITY.de

Click “Add URL Prefix”

Frontend Prefix: /tenant-manager/

Backend Prefix: /

Click OK

Global Request Settings:

Allow External Authorization - Check that this is selected.

IDP App ID: In Keyrock <client_id> of “API Catalogue”

Note:- login to Keyrock and get the API Catalogue OAuth credentials.

Required Roles: Type in "tenant-admin" and click Add

Rate Limit: Unlimited requests

Sub-URL Request Settings:

Click “Add URL Settings” to add the following:

HTTP Method: GET
Regex: ^/
Override required roles from "Global Request Settings"(Check)

Click OK

SAVE
```

### 6.2 Add Token Service Backend

Select `Configuration` -> `API Backends` -> `Add API Backend`

Fill in the information:
```
Name: Token Service

Click “Add Server”

Host: keyrock.fiware-prod.svc.cluster.local

Port: 3000

Click OK

Frontend Host: accounts.fiware.opendata-CITY.de

Backend Host: keyrock.fiware-prod.svc.cluster.local

Click “Add URL Prefix”

Frontend Prefix: /oauth2/password

Backend Prefix: /oauth2/token

Click OK

Global Request Settings:

API Key Checks: Disabled

Rate Limit: Unlimited requests

SAVE
```
### 6.3 Modify Orion Context Broker Backend

Select `Configuration` -> `API Backends`

There should be existing API for **Orion Context Broker.**

Modify the existing API by clicking Orion Context Broker:

```
Change backend host to: orion.fiware-prod.svc.cluster.local

Global Request Settings:

Allow External Authorization - Check that this is selected.


Required Roles: Type in "orion-admin" and click Add

Rate Limit: Unlimited requests

Sub-URL Request Settings:

Click on “Add URL Settings” to add the following:


HTTP Method: Any
Regex: ^/v2/subscriptions
Override required roles from "Global Request Settings"(Check)
HIT OK

HTTP Method: OPTIONS
Regex: ^/v2/.*
API Key Checks: Disabled
Override required roles from "Global Request Settings"(Check)
HIT OK

HTTP Method: Any
Regex: ^/v2/op/notify$
API Key Checks: Disabled
Override required roles from "Global Request Settings"(Check)
HIT OK

HTTP Method: Any
Regex: ^/v2/op/update$
Override required roles from "Global Request Settings"(Check)
HIT OK

HTTP Method: POST
Regex: ^/v2/notify$
Override required roles from "Global Request Settings"(Check)
HIT OK

HTTP Method: DELETE
Regex: ^/v2/.*
Required Headers: fiware-delete: REPLACE_THIS_VALUE
Override required roles from "Global Request Settings"(Check)
HIT OK

SAVE
```
### 6.4 Modify QuantumLeap Backend

Select `Configuration` -> `API Backends`

There should be existing API for **QuantumLeap**.

Modify the existing API:

```
Global Request Settings:

Allow External Authorization - Check that this is selected.

API Key Checks: Required – API keys are mandatory

Required Roles: orion-admin

Rate Limit: Unlimited requests

Override Response Headers (notice 3 lines text):

Access-Control-Allow-Origin: *
Access-Control-Allow-Headers: Authorization, FIWAREService, FIWAREServicePath
Access-Control-Allow-Credentials: true

Sub-URL Request Settings:

Click on “Add URL Settings”

## NOTE: If following issue is still open , https://github.com/smartsdk/ngsi-timeseries-api/issues/332 , we recommend not to use Quantumleap API as open endpoint without added security header. Example can be found below. ##

HTTP Method: GET
Regex: ^/v2/version$
API Key Checks: Disabled
Override required roles from "Global Request Settings"(Check)
HIT OK

SAVE
```

> **_NOTE:_** This Sub-URL setting will replace the existing 'GET'-method, if access to the quantumleap needs to be restricted.

```
HTTP Method: ANY
Regex: ^/
Required Headers: fiware-security: REPLACE_THIS_VALUE
API Key Checks: Disabled
Override required roles from "Global Request Settings"(Check)
HIT OK

SAVE
```



### 6.5 Disable access to API keys via signup

Select `Configuration` -> `API Backends`

Fill in the information:
```
Name: signup config to prevent API key retrieval

Click “Add Server”

Host: apinf.fiware-prod.svc.cluster.local

Port: 6666

Click OK

Frontend Host: umbrella.fiware.opendata-CITY.de

Backend Host: notthere.fiware-prod.svc.cluster.local

Click “Add URL Prefix”

Frontend Prefix: /signup/

Backend Prefix: /signup/

Click OK

Global Request Settings:

Required Roles: admin

SAVE
```

### 6.6 Add Perseo API

Select `Configuration` -> `API Backends`

Fill in the information:
```
Name: Perseo

Click “Add Server”

Host: perseo-fe.fiware-prod.svc.cluster.local

Port: 9090

Click OK

Frontend Host: perseo.fiware.opendata-CITY.de

Backend Host: perseo-fe.fiware-prod.svc.cluster.local

Click “Add URL Prefix”

Frontend Prefix: /

Backend Prefix: /

Click OK

Global Request Settings:

Required Roles: backstop

sub-URL Request Settings:

Click on “Add URL Settings”

HTTP Method: ANY
Regex: ^/rules/.*
Required Headers: perseo-access: ekfrhpgpOEJyudgxxaNT
API Key Checks: Disabled
Override required roles from "Global Request Settings"(Check)
HIT OK

SAVE
```

### 6.7 Add Cosmos API

Here we want to restrict the POST and DELETE operations for the JAR files. 

Select `Configuration` -> `API Backends`

Fill in the information:
```
Name: Cosmos/flink

Click “Add Server”

Host: flink-jobmanager.fiware-prod.svc.cluster.local

Port: 8081

Click OK

Frontend Host: flink.fiware.opendata-CITY.de

Backend Host: flink-jobmanager.fiware-prod.svc.cluster.local

Click “Add URL Prefix”

Frontend Prefix: /

Backend Prefix: /

Click OK

Global Request Settings:

API Key Checks: Disabled - API keys are optional

Required Roles: backstop

sub-URL Request Settings:

Click on “Add URL Settings”

HTTP Method: POST
Regex: ^/.*
Required Headers: cosmos-access: z0wiPrbTNe9Ctt8OxeqT
API Key Checks: Disabled
Override required roles from "Global Request Settings"(Check)
HIT OK

HTTP Method: GET
Regex: ^/.*
Override required roles from "Global Request Settings"(Check)
HIT OK

HTTP Method: HEAD
Regex: ^/.*
Override required roles from "Global Request Settings"(Check)
HIT OK

HTTP Method: OPTIONS
Regex: ^/.*
Override required roles from "Global Request Settings"(Check)
HIT OK

HTTP Method: DELETE
Regex: ^/.*
Required Headers: cosmos-access: z0wiPrbTNe9Ctt8OxeqT
API Key Checks: Disabled
Override required roles from "Global Request Settings"(Check)
HIT OK

SAVE
```

**To publish changes:**

Choose: `Configuration` -> `Publish changes`

... or go to this URL: <https://umbrella.fiware.opendata-CITY.de/admin/#/config/publish>

Check All and **Publish**


**IMPORTANT**: Any changes to platform APIs at this point will overwrite the regex. Unwanted side-effect. 

**IMPORTANT-SECURITY**: Few sub-URL rules (Orion DELETE, Perso ALL, Cosmos POST / DELETE) have required header values. Those should be changed for security! 

Now that the installation and configuration is done, E2E tests can be executed as described in [platform-testing](../platform-testing/README.md).

## 7. Configure Knowage

Website backend and Keyrock application need to be done before these steps:

### 7.1 Knowage secret
Fill in the following information:

* **CLIENT_ID**: In Keyrock, Application Knowage, OAuth-Credentials
* **CLIENT_SECRET**: In Keyrock, Application Knowage, OAuth-Credentials
* **KEYROCK-ADMIN-EMAIL**: Created within the secret "idm-secret" in chapter 1.2
* **KEYROCK-ADMIN-PASSWORD**: Created within the secret "idm-secret" in chapter 1.2

And execute the command:
```
sudo kubectl create secret generic knowage-secret \
	--from-literal=hmacKey=randomKey \
	--from-literal=keyrockClientId=CLIENT_ID \
	--from-literal=keyrockSecret=CLIENT_SECRET \
	--from-literal=keyrockRolesPath=CLIENT_ID/roles \
	--from-literal=keyrockApplicationId=CLIENT_ID \
	--from-literal=keyrockAdminId=admin \
	--from-literal=keyrockAdminEmail=KEYROCK_ADMIN_EMAIL \
	--from-literal=keyrockAdminPassword=KEYROCK_ADMIN_PASSWORD \
	-n fiware-prod --kubeconfig /opt/testing/prodkube.config
```

### 7.2 Knowage deployment step #1

Knowage deployment needs to be done in two stages. Deploying new branch starts a job that deploys Knowage for configuration.

* Knowage (https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/knowage)

Wait until first stage has PASSED and wait until Knowage is up and running (2-5 minutes).

### 7.3 Knowage configuration
Head to your Knowage address: https://knowage.fiware.opendata-CITY.de/knowage Login as "biadmin"/biadmin

Menu -> Configuration Management -> Change 'value check' of following settings:  
![img](platform-setup/img/knowage_configuration.png)

> **_NOTE:_** Search for the keyword, press the edit-icon (pencil) and alter the value.

```
SPAGOBI_SSO.ACTIVE = true
SPAGOBI.SECURITY.PORTAL-SECURITY-CLASS.className = it.eng.spagobi.security.OAuth2SecurityInfoProvider
SPAGOBI.SECURITY.USER-PROFILE-FACTORY-CLASS.className = it.eng.spagobi.security.OAuth2SecurityServiceSupplier
SPAGOBI_SSO.SECURITY_LOGOUT_URL = https://accounts.fiware.opendata-CITY.de
```

Logout from Knowage.

### 7.4 Knowage deployment step #2

Return to gitlab Pipeline and press 'play' on the manual process, to deploy second stage. Once this stage has 'PASSED' wait until Knowage is up and running (2-5 minutes).

> **_NOTE:_** This will re-deploy Knowage with other image.

### 8 Final Test

Redo test from chapter 3.1 with Knowage included now

The script tests the connectivity from Platform to each component, by checking open port from each component.
```
sudo kubectl -n fiware-prod --kubeconfig /opt/testing/prodkube.config exec -it $(kubectl get pod -l app=apinf -o jsonpath="{.items[0].metadata.name}" -n fiware-prod --kubeconfig /opt/testing/prodkube.config) -- bash -c "apt update; apt install netcat -y; echo; echo  TESTING; timeout 4 nc mongo 27017 -vw2; timeout 4 nc orion 1026 -vw2; timeout 4 nc umbrella 443 -vw2; timeout 4 nc umbrella-elasticsearch 9200 -vw2; timeout 4 nc quantumleap 8668 -vw2; timeout 4 nc quantumleapcrate 4200 -vw2; timeout 4 nc keyrock 3000 -vw2; timeout 4 nc grafana 3000 -vw2; timeout 4 nc kurento 8888 -vw2; timeout 4 nc perseo-core 8081 -vw2; timeout 4 nc perseo-fe 9090 -vw2; timeout 4 nc flink-jobmanager 8081 -vw2; timeout 4 nc knowage 8082 -vw2; echo END TEST"
```
The output will be similar to this:
```
Hit:1 http://security.debian.org/debian-security stretch/updates InRelease
Ign:2 http://deb.debian.org/debian stretch InRelease
Hit:3 http://deb.debian.org/debian stretch-updates InRelease
Hit:4 http://deb.debian.org/debian stretch Release
Reading package lists... Done
Building dependency tree
Reading state information... Done
16 packages can be upgraded. Run 'apt list --upgradable' to see them.
Reading package lists... Done
Building dependency tree
Reading state information... Done
netcat is already the newest version (1.10-41).
0 upgraded, 0 newly installed, 0 to remove and 16 not upgraded.

TESTING
mongo.fiware-prod.svc.cluster.local [192.168.179.49] 27017 (?) open

orion.fiware-prod.svc.cluster.local [192.168.208.246] 1026 (?) open

umbrella.fiware-prod.svc.cluster.local [192.168.174.152] 443 (?) open

umbrella-elasticsearch.fiware-prod.svc.cluster.local [192.168.155.252] 9200 (?) open

quantumleap.fiware-prod.svc.cluster.local [192.168.209.209] 8668 (?) open

quantumleapcrate.fiware-prod.svc.cluster.local [192.168.210.62] 4200 (?) open

keyrock.fiware-prod.svc.cluster.local [192.168.151.137] 3000 (?) open

grafana.fiware-prod.svc.cluster.local [192.168.164.156] 3000 (?) open

kurento.fiware-prod.svc.cluster.local [192.168.242.209] 8888 (?) open

perseo-core.fiware-prod.svc.cluster.local [192.168.160.65] 8081 (?) open

perseo-fe.fiware-prod.svc.cluster.local [192.168.175.45] 9090 (?) open

flink-jobmanager.fiware-prod.svc.cluster.local [192.168.174.247] 8081 (?) open

knowage.fiware-prod.svc.cluster.local [192.168.203.126] 8082 (?) open

END TEST
```
Please check, that every Port is in 'Open' state.

