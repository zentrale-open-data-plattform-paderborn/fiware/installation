# Cosmos Configuration
This document describes the process of setting up Cosmos after the deployment  is done. 

> **_NOTE:_**  Here we note out only differences to official [Cosmos Documentation](https://fiware-cosmos.readthedocs.io/en/latest/)


## Prerequisites

* Apache Flink / Cosmos  is deployed
* Postman is installed
* This [Collection](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/master/howtos/Collections/Cosmos.postman_collection.json) is imported to Postman.
* JAR file for upload is available.


## 1. Short description of work flow

To upload JAR files (Orion-Flink connector for example) user needs a Header (cosmos-access) with secret value on the request. This secret should be defined in Umbrella. This effectively will preventusers form uploading files with flink UI.

Given that the secret value is in place, instead of using the UI, you need to use Postman /CURL.

### 1.1 Upload JAR file

1. In Postman open the Request "Cosmos -> CosmosPOSTJAR"
2. In the Headers Tab, make sure that the `cosmos-access` header value matches installation.
3. In the Body Tab, change the upload file to desired.
4. Press `Send`, to send the request to upload.

* Expected: The response status code should be 200 ok with similar body:

```
{
    "filename": "/tmp/flink-web-a9f0b924-fa07-421c-8ed3-f821975060f6/flink-web-upload/783cc6bf-09dc-433b-9f8d-12bbd78bd5ed_orion.flink.connector.examples-1.2.3.jar",
    "status": "success"
}
```

> **_NOTE:_**  Copy the filename part (e.g. `783cc6bf-09dc-433b-9f8d-12bbd78bd5ed_orion.flink.connector.examples-1.2.3.jar`) of the response, it will come in handy in the next step

In addition to this, the file should be visible in the "Submit New Job" tab in https://flink.fiware.opendata-CITY.de/#/submit

### 1.2 Delete JAR file

1. In Postman open the Request "Cosmos -> CosmosDELETEJAR"
2. In the Headers Tab, make sure that the `cosmos-access` header value matches installation.
3. In the URL of the request replcate the text `INTERNAL_JARFILENAME_HERE` with correct value. This can be obtained by recording the file name from the upload response or by observing the  request via browser tools (network tab) when making the DELETE request from the UI.
4. Press `Send`, to send the request to upload.

* Expected: The response status code should be 200 OK and should have no body.

In addition to this, the file should not be visible in the "Submit New Job" tab in https://flink.fiware.opendata-CITY.de/#/submit

## 2. Known Problems

[Cosmos Github project](https://github.com/ging/fiware-cosmos) states as Medium term goal Docker images (next 9 months after next planned release). Since there are no official docker images, we had to resort to the deployment configuration described above.

