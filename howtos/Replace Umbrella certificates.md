**This instruction assumes that you have control of fiware-prod namespace, without a kubeconfig file, and new certificate files (.crt and .ca-bundle) for Umbrella. Deployment SSL certificates follow Nginx format, with "cert" being the first entry in 'fullchain'-certificate.**



Delete old certificate fullchain.pem and privkey from Kubernetes secrets, in fiware-prod namespace.
```
kubectl delete secret umb-fullchain -n fiware-prod
kubectl delete secret umb-privkey -n fiware-prod
```

Create fullchain.pem from using .crt and & .ca-bundle certificate files.
```
cat New.crt New.ca-bundle > fullchain.pem
```

Put new fullchain and privkey into kubernetes secrets.
```
kubectl create secret generic umb-fullchain --from-file=fullchain.pem -n fiware-prod
kubectl create secret generic umb-privkey --from-file=privkey.pem -n fiware-prod
```

Restart Umbrella by deleting the pod. Command below fetches umbrella pod name automatically, in fiware-prod namespace, with nested Kubectl command.
```
kubectl delete pod $(kubectl get pod -l io.kompose.service=umbrella -o jsonpath="{.items[0].metadata.name}" -n fiware-prod) -n fiware-prod
```