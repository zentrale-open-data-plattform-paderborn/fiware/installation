# Perseo Configuration
This document describes the process of setting up Perseo after the deployment  is done. 

> **_NOTE:_**  Here we not out only differences to official [Perseo Documentation](https://perseo.readthedocs.io/en/latest/), if any.


## Prerequisites

* Perseo is deployed
* Configuration in Umbrella is done
* Postman is installed
* [Postman collection](https://gitlab.com/zentrale-open-data-plattform-paderborn/fiware/installation/-/blob/master/howtos/Collections/Perseo.postman_collection.json) is imported to Postman.
* A user is available which can create a Tenant

## 1. Short description of work flow

The following steps will outline how to do a minimal test with the installation. We'll create a Tenant, create a subscription, a rule and test it by updating data.

### 1.1 Create a Tenant

Go to Api Management with an exisiting user which has been granted a tenant-admin role. Create a tenant for tests / configuration:

1. Click `Add Tenant`
2. Enter a tenant name `perseo` and a description
3. Click `Add Tenant` to save.
* Expected: Tenant was created without errors and is visible in the list of tenants.

> **_NOTE:_**  If you use a tenant name other than "perseo", please remember the name - it's needed in the next chapter when creating a subscription.

### 1.2 Create a subscription

> **_NOTE:_**  Use the formerly created servicename in Header.

1. In Postman open the Request "Perseo -> PerseoPOSTSubscription"
2. In the Headers Tab, insert your bearer token
3. Press `Send`, to send the request to orion.

* Expected: The response status code should be 201 Created and should have no body.

### 1.3 Post a rule

> **_NOTE:_**  Use the formerly created servicename in Header.

1. In Postman open the Request "Perseo -> PerseoPOSTRule"
2. In the body replace `xxxx@yyy.de` with valid email address where the test email can go.
3. Press `Send`, to send the request to Perseo.

* Expected: The response status code should be 200 OK

### 1.4 Post data

> **_NOTE:_**  Use the formerly created servicename in Header.

1. In Postman open the Request "Perseo -> PerseoPOSTData"
2. In the Headers Tab, insert your bearer token
3. Press `Send`, to send the request to orion.

* Expected: The response status code should be 204 No context.
* Expected: After a while, email should appear.


### 2. Known issues

Email sending is  working via a hack. This hack is not recommended to be used in other deployments. (we are using STARTTLS & email server requires username and password): https://stackoverflow.com/questions/62127112/perseo-events-do-not-seen-to-fire-with-ngsi-v2 and https://github.com/telefonicaid/perseo-fe/issues/272
