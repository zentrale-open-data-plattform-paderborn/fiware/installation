# 1. Introduction

Backing up databases requires graceful shutdown of each database container. Before these databases are shutdown, it is advisable to shutdown containers using the databases.

Graceful shutdown of containers can be done by changing the container replicas to zero.

Examples of scaling down deployment and statefulset.

```
kubectl scale --replicas=0 deployment umbrella
kubectl scale --replicas=0 statefulset grafana
```

# 2. Halting the System
## 2.1 Scaling down Services
Before scaling down databases, please scale down following deployments and statefulsets:

umbrella
apinf
tenantmanager
keyrock
orion
knowage
perseo
-perseo-fe
-perseo-core
quantumleap
cosmos/flink
-flink-jobmanager (statefulset)
-flink-taskmanager
grafana (statefulset)

### 2.1.1 C&P
Commands to scale down components in right order:

```
kubectl scale --replicas=0 deployment umbrella
kubectl scale --replicas=0 deployment apinf
kubectl scale --replicas=0 deployment tenantmanager
kubectl scale --replicas=0 deployment keyrock
kubectl scale --replicas=0 deployment orion
kubectl scale --replicas=0 deployment knowage
kubectl scale --replicas=0 deployment perseo-fe
kubectl scale --replicas=0 deployment perseo-core
kubectl scale --replicas=0 deployment quantumleap
kubectl scale --replicas=0 deployment flink-taskmanager
kubectl scale --replicas=0 statefulset flink-jobmanager
kubectl scale --replicas=0 statefulset grafana
```

## 2.2 Scaling down Database
Then scale down the databases:

mongo (statefulset)
quantumleapcrate (statefulset)
umbrella-elasticsearch (statefulset)

### 2.2.1 C&P
```
kubectl scale --replicas=0 statefulset mongo
kubectl scale --replicas=0 statefulset quantumleapcrate
kubectl scale --replicas=0 statefulset umbrella-elasticsearch
```

Services and databases are now stopped and with no more traffic on the volumes.

# 3. Do any needed Actions on the Volumes
(Out of scope of this documentation, e.g. backup or restore volumes)

# 4. Restart the System
To restart stopped components scale up previously mentioned components in reverse order. Flink-taskmanager uses 2 replicas, others use just 1.

Example of scaling up deployment and statefulset:

```
kubectl scale --replicas=1 deployment umbrella
kubectl scale --replicas=1 statefulset grafana
```

## 4.1 Restart databases

### 4.1.1 C&P

```
kubectl scale --replicas=1 statefulset umbrella-elasticsearch
kubectl scale --replicas=3 statefulset quantumleapcrate
kubectl scale --replicas=1 statefulset mongo
```

## 4.2 Restart services
### 4.2.1  C&P

```
kubectl scale --replicas=1 statefulset grafana
kubectl scale --replicas=1 statefulset flink-jobmanager
kubectl scale --replicas=2 deployment flink-taskmanager
kubectl scale --replicas=5 deployment quantumleap
kubectl scale --replicas=1 deployment perseo-core
kubectl scale --replicas=1 deployment perseo-fe
kubectl scale --replicas=1 deployment knowage
kubectl scale --replicas=1 deployment orion
kubectl scale --replicas=1 deployment keyrock
kubectl scale --replicas=1 deployment tenantmanager
kubectl scale --replicas=1 deployment apinf
kubectl scale --replicas=1 deployment umbrella
```

> Note: If there are more components in the future (outside components that use these databases), these need to be taken into account as well.
