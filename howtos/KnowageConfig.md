# Knowage Configuration
This document describes the process of setting up Knowage after the deployment and Keyrock Application is done. 

> **_NOTE:_**  Here we note out only differences to official [Knowage Documentation](https://knowage.readthedocs.io/en/latest/)


## Prerequisites

* Website backend is configured.
* Application is configured in Keyrock.
* Knowage is double deployed (means that the deployment is completed).

## 1. Login to Knowage

Navigate to https://knowage.fiware.opendata-CITY.de/knowage

Sign-In with Admin (Keyrock admin).

## 2. Configure roles

Menu->Knowage Icon Dropdown menu->Select "My Roles". Select "Admin" as default role.

SAVE

Click the hamburger menu, navigate to "Roles Management" under PROFILE MANAGEMENT.
Click Role (Admin) you want to modify and give the roles under AUTHORIZATIONS tab, "Items for user menu" section:

![image](images/knowage-roles2.PNG)

SAVE

## 3. Configure cache

Select Menu-> data source -> Plus icon. Fill in the following details:

![image](images/knowage-cache.PNG)

Hit Test to verify successful configuration.

SAVE

Logout from Knowage and Keyrock.

More information on the cache configuration: [Configure cache](https://github.com/KnowageLabs/Knowage-Server-Docker/issues/5)

## 4. Create Orion Context Broker connection

Login with knowageUser. Select Menu->Knowage Icon Dropdown menu->Select "My Roles". Select "Admin" as default role.

SAVE

Please follow "data set creation" section in the instructions:

https://knowage.readthedocs.io/en/latest/user/NGSI/README/index.html


Specify `fiware-service`, as we are in multitenant mode:

![detail](images/staging-knowage-cb2.PNG)

Hit Preview to see if data is as expected.

SAVE

## 5. Create a Widget (Table as example)
Click on the "Menu" -> "Workspace" as shown in below picture:

![image](images/Screenshot11.49.19.png)

Click on the "Analysis"

![image](images/Screenshot11.50.20.png)

Click on the "+" icon on the top right corner and select sub-menu option "Cockpit"

![image](images/Screenshot11.51.11.png)

Click on the red 3 line cicle on the top right corner and click "+" to add widget

![image](images/Screenshot11.51.37.png)

The table will appear below with many widget options. Select "Table" widget

![image](images/Screenshot12.09.56.png)

Click on the "+" icon next to Dataset and select the dataset

![image](images/Screenshot11.52.21.png)

Click "SAVE" in the dialog at the bottom right.

Click "SAVE" (disc) via hamburger-menu to save the cockpit.

![image](images/knowage_hamburger.png)

You should see a following table:

![table](images/knowage-table.PNG)