Steps to clean up Fiware-Prod.  
For other namespaces, change prod with dev or staging in each command

Pre-requisites: 

1. Kubeconfig for the cluster
2. Access to mysql-master 
3. mysql-master root password
    - or other means to delete 1 or more databases and create 1.
4. This instruction assumes that you have control of fiware-prod namespace, without a kubeconfig file

#Connect to fiware-prod-mysql-master using root user and password.
mysql -u root -p -h fiware-prod-mysql-master

show databases;
mysql> drop database `idm-fiware-prod`;
mysql> drop database `knowage-fiware-prod`;
mysql> create database `knowage-fiware-prod`;

#Log out of Mysql.

#Run following commands to finish clean up process:

#Delete deployments and statefulsets
kubectl -n fiware-prod  delete deployments.apps apinf
kubectl -n fiware-prod  delete deployments.apps keyrock
kubectl -n fiware-prod  delete deployments.apps kurento
kubectl -n fiware-prod  delete deployments.apps orion
kubectl -n fiware-prod  delete deployments.apps perseo-core
kubectl -n fiware-prod  delete deployments.apps perseo-fe
kubectl -n fiware-prod  delete deployments.apps quantumleap
kubectl -n fiware-prod  delete deployments.apps tenantmanager
kubectl -n fiware-prod  delete deployments.apps umbrella
kubectl -n fiware-prod  delete deployments.apps knowage
kubectl -n fiware-prod  delete deployments.apps flink-taskmanager
kubectl -n fiware-prod  delete statefulsets.apps flink-jobmanager
kubectl -n fiware-prod  delete statefulsets.apps quantumleapcrate
kubectl -n fiware-prod  delete statefulsets.apps mongo
kubectl -n fiware-prod  delete statefulsets.apps umbrella-elasticsearch
kubectl -n fiware-prod  delete statefulsets.apps grafana

#Delete services
kubectl -n fiware-prod  delete svc apinf
kubectl -n fiware-prod  delete svc keyrock
kubectl -n fiware-prod  delete svc kurento
kubectl -n fiware-prod  delete svc orion
kubectl -n fiware-prod  delete svc perseo-core
kubectl -n fiware-prod  delete svc perseo-fe
kubectl -n fiware-prod  delete svc quantumleap
kubectl -n fiware-prod  delete svc tenantmanager
kubectl -n fiware-prod  delete svc umbrella
kubectl -n fiware-prod  delete svc knowage
kubectl -n fiware-prod  delete svc quantumleapcrate
kubectl -n fiware-prod  delete svc mongo
kubectl -n fiware-prod  delete svc umbrella-elasticsearch
kubectl -n fiware-prod  delete svc grafana
kubectl -n fiware-prod  delete svc flink-jobmanager
kubectl -n fiware-prod  delete svc flink-taskmanager

#Delete persistent volume claims
kubectl -n fiware-prod  delete pvc fiware-prod-crate-clusterdata-quantumleapcrate-0
kubectl -n fiware-prod  delete pvc fiware-prod-crate-clusterdata-quantumleapcrate-1
kubectl -n fiware-prod  delete pvc fiware-prod-crate-clusterdata-quantumleapcrate-2
kubectl -n fiware-prod  delete pvc fiware-prod-esdata-umbrella-elasticsearch-0
kubectl -n fiware-prod  delete pvc fiware-prod-grafanadata-grafana-0
kubectl -n fiware-prod  delete pvc fiware-prod-mongodata-mongo-0
kubectl -n fiware-prod  delete pvc fiware-prod-flinkdata-flink-jobmanager-0

#Delete cronjobs
kubectl -n fiware-prod  delete cronjobs.batch harvester-cronjob

#Delete secrets
kubectl -n fiware-prod  delete secret email-secret
kubectl -n fiware-prod  delete secret grafana-password
kubectl -n fiware-prod  delete secret idm-secret
kubectl -n fiware-prod  delete secret maxmind-license
kubectl -n fiware-prod  delete secret tenantmanager-credentials
kubectl -n fiware-prod  delete secret gitlab-data-models-registry
kubectl -n fiware-prod  delete secret knowage-secret
kubectl -n fiware-prod  delete secret perseo-config

## Warning ##

#Umbrella certificates are handled by the host, but for re-deployment they do not need to be deleted. If they are not deleted, Umbrella certificate secrets do not need to be recreated during system setup.
kubectl -n fiware-prod  delete secret umb-fullchain
kubectl -n fiware-prod  delete secret umb-privkey